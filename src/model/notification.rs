use std::fmt::{Display, Formatter, Result};
use rocket::serde::{Serialize, Deserialize};

#[derive(Debug, Clone, Deserialize, Serialize)]
#[serde(crate = "rocket::serde")]

pub struct Notification {
    #[serde(skip_deserializing)]
    pub product_title: String,
    pub product_type: String,
    pub product_url: String,
    pub subscriber_name: String,
    pub status: String,
}